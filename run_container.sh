#!/bin/bash -l

module purge
module load apptainer/1.3.2

# Execute a command (here python3) within the container, mount host filesystems
apptainer --quiet exec --bind /u:/u,/ptmp:/ptmp apptainer_conda_image.sif \
    python3 -c 'import numpy as np; print(f"Hello from NumPy {np.__version__}")'

# Run the %runscript from the definition file, mount host filesystems
#apptainer --quiet run  --bind /u:/u,/ptmp:/ptmp apptainer_conda_image.sif

