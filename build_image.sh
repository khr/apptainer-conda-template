#!/bin/bash -l

# Example showing how-to build an Apptainer image (.sif)
# from a definition file (.def) on MPCDF systems.


module purge
module load apptainer/1.3.2

apptainer build apptainer_conda_image.sif apptainer.def

