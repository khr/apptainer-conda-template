# Apptainer-Conda-Template

## Introduction

This demo shows how to create a compressed Apptainer image containing a Conda environment.
The Conda environment is defined via the file `environment.yml`.

Presented during the MeetMPCDF seminar on Oct 10, 2024.

## Usage

1. Replace `environment.yml` with your own Conda environment definition file. (Alternatively, just keep it to perform a test.)
2. Run `build_image.sh` to create a compressed Apptainer image containing the Conda environment.
3. The script `run_container.sh` shows how to execute commands from the Conda environment, adapt it to your needs.

